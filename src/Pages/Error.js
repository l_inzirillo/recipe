import "./../App.css";
import Errorimg from "./../Images/Errorimg.gif";

function Error() {
  return (
    <>
      <h2>Whats wrong ?</h2>
      <img src={Errorimg} alt="toto" />
    </>
  );
}

export default Error;
