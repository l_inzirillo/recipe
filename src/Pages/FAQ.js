import Paragraphe from "../Component/Paragraphe";
import SousTitre from "../Component/SousTitre";
import Titre from "../Component/Title";
import "./../App.css";
function FAQ() {
  return (
    <div className="FAQ">
      <Titre nomDuTitre="Bienvenue sur notre planète" />
      <SousTitre
        texteSousTitre={"Comment trouver la recette que l'on cherche ?"}
      />
      <Paragraphe
        texteParagraphe={"Lorem ipsum recetta findare Mozarella Deviendrez"}
      />

      <SousTitre texteSousTitre={"Quelle est la meilleure pizza ?"} />
      <Paragraphe texteParagraphe={"La pizza hawaïenne sans hésiter !"} />

      <SousTitre texteSousTitre={"Quel est le meilleur fromage ?"} />
      <Paragraphe texteParagraphe={"La parmesan sans hésiter !"} />
    </div>
  );
}
/*
 */
export default FAQ;
