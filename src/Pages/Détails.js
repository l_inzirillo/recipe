import { useMemo } from "react";
import { useLocation } from "react-router-dom";
import faker from "@faker-js/faker";
import Titre from "../Component/Title";
import "./../App.css";
function useQuery() {
  const { search } = useLocation();
  return useMemo(() => new URLSearchParams(search), [search]);
}
function Details({ nom, lienImage, ingredients, Steps }) {
  let query = useQuery();
  let comment = [];
  let randomNumber = Math.floor(Math.random() * 1);
  for (let i = 0; i < randomNumber; i++) {
    comment.push(
      faker.name.findName() +
        "-" +
        faker.internet.email() +
        ": " +
        faker.internet.emoji()
    );
  }

  return (
    <div>
      <Titre nomDuTitre={query.get("nom")} />
      <img src={query.get("lienImage")} alt="lienImage" />

      <ul>
        {query
          .get("ingredients")
          .split("|")
          .map((element) => (
            <li>{element}</li>
          ))}
      </ul>
      <ul>
        {query
          .get("Steps")
          .split("|")
          .map((element) => (
            <li>{element}</li>
          ))}
      </ul>
      <h2>Commentaires</h2>
      {comment.map((el) => (
        <p>{el}</p>
      ))}
    </div>
  );
}
export default Details;
