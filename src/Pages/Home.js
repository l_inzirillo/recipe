import Plat from "./../Component/Plat";
import "./../App.css";
import salade from "./../Images/salade.jpg";
import BarreDeRecherche from "./../Component/barreDeRecherche";
import Data from "./../bdd.json";
import { useMemo, useState } from "react";
import { Link, useLocation } from "react-router-dom";
function Home() {
  const [Text, setText] = useState();
  const [Tab, setTab] = useState([]);
  function searchFor(Text = "Hello") {
    function useQuery() {
      const { search } = useLocation();
      return useMemo(() => new URLSearchParams(search), [search]);
    }
    let Tableau = [];
    Data.map((recipe) => {
      if (recipe.RecipeName.search(Text) > -1) {
        Tableau.push(recipe);
      }
      return Text;
    }, Tableau);
    return Tableau;
  }
  //+"&Steps"+element.Steps_
  return (
    <div className="App">
      <h1 classeName="tittleHome">
        Réveillez vos papilles gustatives et laissez votre porte-monnaie se
        reposer.
      </h1>
      <p>Nombre de résultats : {Tab.length}</p>
      <BarreDeRecherche
        submit={(event) => {
          setTab([...searchFor(Text)]);
        }}
        onChange={() => {
          setTab([]);
          setText(Text);
          searchFor(Text);
        }}
        value={Text}
        updateText={(changeText) => setText(changeText)}
      />
      <div className="containerHome">
        {Tab.map((element) => (
          <Link
            to={
              "/Details?nom=" +
              element.RecipeName +
              "&lienImage=" +
              salade +
              "&ingredients=" +
              element.Ingredients_ +
              "&Steps=" +
              element.Steps_
            }
          >
            <Plat
              title={element.RecipeName}
              Category={element.Category}
              lienImage={salade}
              Time={element.Duration}
              Ingredients={element.Ingredients_}
              Personnes={element.NBpersons_}
            />
          </Link>
        ))}
        <Plat
          title={"Greek salade"}
          Category={"Plat"}
          lienImage={salade}
          Time={"1h30"}
          Personnes={"2 Personnes"}
        />
      </div>
    </div>
  );
}

export default Home;
