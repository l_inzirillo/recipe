function Titre({ nomDuTitre }) {
  return (
    <div>
      <h1 className="Titre">{nomDuTitre}</h1>
    </div>
  );
}
export default Titre;
