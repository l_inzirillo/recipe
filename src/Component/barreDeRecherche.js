function BarreDeRecherche({ submit, value, updateText, onChange }) {
  return (
    <div>
      <form
        onSubmit={() => {
          submit();
        }}
      >
        <input
          className="BarreDeRecherche"
          value={value}
          onChange={(event) => {
            updateText(event.target.value);
            submit();
          }}
        />
        <input type="submit" value="Envoyer" />
      </form>
    </div>
  );
}
export default BarreDeRecherche;
