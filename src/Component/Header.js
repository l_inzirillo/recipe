import { Link } from "react-router-dom";

function Header() {
  return (
    <header>
      <nav className="Navbarre">
        <Link className="TextNav" to="/">
          Home
        </Link>
        <Link className="TextNav" to="/FAQ">
          FAQ
        </Link>
        <Link className="TextNav" to="/Error">
          Error
        </Link>
      </nav>
    </header>
  );
}

export default Header;
