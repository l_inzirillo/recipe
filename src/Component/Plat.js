function Plat({ title, lienImage, Time, Personnes, Category, Ingredients }) {
  return (
    <div className="Plat">
      <h1 className="TittlePlat">{title}</h1>
      <h3 className="CategorylePlat">{Category}</h3>
      <img className="ImagePlat" src={lienImage} alt={title} />
      <p className="TimelePlat">{Time}</p>
      <p className="TimelePlat">{Ingredients}</p>
      <p className="PersonneslePlat">{Personnes}</p>
    </div>
  );
}
export default Plat;
