import "./App.css";
import { Routes, Route } from "react-router-dom";
import Home from "./Pages/Home";
import Error from "./Pages/Error";
import FAQ from "./Pages/FAQ";
import Header from "./Component/Header";
import Details from "./Pages/Détails";

function App() {
  return (
    <div className="App">
      <Header> </Header>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="*" element={<Error />} />
        <Route path="/FAQ" element={<FAQ />} />
        <Route path="/Details" element={<Details />} />
      </Routes>
    </div>
  );
}

export default App;
